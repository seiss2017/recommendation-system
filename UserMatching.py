import numpy as np
import math
import cvxopt
import csv

def cal_distance_matrix(feature_matrix):
    row_num=feature_matrix.shape[0]
    # col_num=feature_matrix.shape[1]
    distance_matrix=np.zeros((row_num,row_num))
    for row in range(0,row_num):
        for col in range(row+1,row_num):
            distance_matrix[row,col]=math.sqrt(sum(np.power((feature_matrix[row,:]-feature_matrix[col,:]),2)))
            distance_matrix[col,row]=distance_matrix[row,col]
        distance_matrix[row,row]=row_num
    return distance_matrix

def cal_neighbors(distance_matrix,neighbor_num):
    # print distance_matrix
    sorted_distance_matrix=np.argsort(distance_matrix,axis=1)
    row_num = sorted_distance_matrix.shape[0]
    col_num = sorted_distance_matrix.shape[1]
    nearst_neighbor_matrix=np.zeros((row_num,col_num))
    index_matrix=sorted_distance_matrix[:,0:neighbor_num]
    for i in range(0,row_num):
        nearst_neighbor_matrix[i,index_matrix[i,:]]=1
    return nearst_neighbor_matrix

def get_LN_similarity_matrix(feature_matrix,nearst_neighbor_matrix,tag,regulation):
    k=int(feature_matrix.shape[0])
    similairty_matrix=np.zeros((k,k))
    if(tag==1):
        k=1
    for i in range(0,k):
        nearst_neighbors_index=np.where(np.transpose(nearst_neighbor_matrix[i,:])==1)
        nearst_neighbors=feature_matrix[nearst_neighbors_index]
        neighbor_num=nearst_neighbors.shape[0]
        G1=np.kron(np.ones((neighbor_num,1)),feature_matrix[i,:])-nearst_neighbors
        G2=np.transpose(np.kron(np.ones((neighbor_num,1)),feature_matrix[i,:]))-np.transpose(nearst_neighbors)
        if(regulation=='regulation_1'):
            G_i=np.matrix(G1)*np.matrix(G2)
        if(regulation=='regulation_2'):
            G_i = np.matrix(G1)*np.matrix(G2)
            G_i = G_i + 0.01 * sum(np.diag(G_i)) * np.identity(neighbor_num)
        else:
            raise ValueError
        P= cvxopt.matrix(2*G_i)
        if(G_i.max()!=0):
            q = cvxopt.matrix(np.zeros((neighbor_num,1)))
            A=cvxopt.matrix(np.ones((neighbor_num,1)).T)
            b=cvxopt.matrix(np.ones((1,1)))
            G=cvxopt.matrix(-np.identity(neighbor_num))
            h=cvxopt.matrix(np.zeros((neighbor_num,1)))
            sol = cvxopt.solvers.qp(P=P,q=q,G=G,h=h,A=A,b=b)
            w=np.transpose(sol['x'])
        else:
            w=np.zeros((1,neighbor_num))
        if (w.min() < 0):
            w[w < 0] = 0
        similairty_matrix[i,nearst_neighbors_index]=w
        print i
    for i in range(0, similairty_matrix.shape[0]):
        similairty_matrix[i, i] = 0
    similairty_matrix=np.matrix(similairty_matrix)
    similairty_matrix=normalize_similarity_matrix(similairty_matrix)
    return  similairty_matrix

def get_Jaccard_similarity_matrix(feature_matrix):
    feature_matrix=np.matrix(feature_matrix)
    intersection_matrix=feature_matrix*(feature_matrix.T)
    degree_matrix=sum(feature_matrix.T)
    row_matrix=degree_matrix.T *np.ones((1,degree_matrix.shape[0]))
    col_matrix=row_matrix.T
    union_matrix=col_matrix+row_matrix-intersection_matrix
    with np.errstate(all='ignore'):
        Jaccard_similarity_matrix = np.divide(np.array(intersection_matrix, float), union_matrix)
    Jaccard_similarity_matrix[np.isnan(Jaccard_similarity_matrix)] = 0
    for i in range(0, Jaccard_similarity_matrix.shape[0]):
        Jaccard_similarity_matrix[i, i] = 0
    Jaccard_similarity_matrix=normalize_similarity_matrix(Jaccard_similarity_matrix)
    return Jaccard_similarity_matrix

def get_Sorenson_similarity_matrix(feature_matrix):
    feature_matrix = np.matrix(feature_matrix)
    neighbor_matrix = sum(feature_matrix.T)
    row_matrix = np.multiply(neighbor_matrix, np.ones((len(neighbor_matrix), 1), int))
    column_matrix = np.transpose(row_matrix)
    union_matrix = row_matrix + column_matrix
    intersection_matrix = feature_matrix * feature_matrix.T
    Sorenson_similarity_matrix=np.divide(2.0 * intersection_matrix, union_matrix)
    for i in range(0, Sorenson_similarity_matrix.shape[0]):
        Sorenson_similarity_matrix[i, i] = 0
    Sorenson_similarity_matrix=normalize_similarity_matrix(Sorenson_similarity_matrix)
    return Sorenson_similarity_matrix

def normalize_similarity_matrix(raw_similarity_matrix):
    row_sum_vector=np.squeeze(np.asarray(sum(raw_similarity_matrix)))
    row_sum_denominator=np.linalg.pinv(np.diag(row_sum_vector))
    similarity_matrix=row_sum_denominator*raw_similarity_matrix
    return similarity_matrix

def get_Coisine_similarity_matrix(feature_matrix):
    feature_matrix = np.matrix(feature_matrix)
    neighbor_matrix = np.matrix(sum(feature_matrix.T))
    neighbor_matrix = np.multiply(np.transpose(neighbor_matrix), neighbor_matrix)
    neighbor_matrix = np.power(neighbor_matrix, 0.5)
    intersection_matrix = feature_matrix * feature_matrix.T
    Cosine_similarity_matrix = np.divide(intersection_matrix, neighbor_matrix)
    for i in range(0, Cosine_similarity_matrix.shape[0]):
        Cosine_similarity_matrix[i, i] = 0
    Cosine_similarity_matrix=normalize_similarity_matrix(Cosine_similarity_matrix)
    return Cosine_similarity_matrix

def get_users_matching_result(similarity_matrix):
    sorted_similarity_matrix=np.argsort(-similarity_matrix,axis=1)
    return sorted_similarity_matrix

def get_max_matrix(matrix_A, matrix_B):
    tmp = matrix_A - matrix_B
    tmp[tmp < 0] = 0
    return tmp + matrix_B

def UserMatching(feature_matrix):
    distance_matrix = cal_distance_matrix(feature_matrix)
    total_user_number=feature_matrix.shape[0]
    nearst_neighbor_matrix = cal_neighbors(distance_matrix, total_user_number/2)
    LN_similarity_matrix = get_LN_similarity_matrix(feature_matrix, nearst_neighbor_matrix, 0, 'regulation_2')
    Jaccard_similarity_matrix=get_Jaccard_similarity_matrix(feature_matrix)
    Cosine_similarity_matrix=get_Coisine_similarity_matrix(feature_matrix)
    Sorenson_similarity_matrix=get_Sorenson_similarity_matrix(feature_matrix)
    # print LN_similarity_matrix
    # print Jaccard_similarity_matrix
    # # print Cosine_similarity_matrix
    # # print Sorenson_similarity_matrix
    max_1=get_max_matrix(LN_similarity_matrix,Jaccard_similarity_matrix)
    max_2=get_max_matrix(max_1,Cosine_similarity_matrix)
    max_3=get_max_matrix(max_2,Sorenson_similarity_matrix)
    integrated_similarity_matrix=normalize_similarity_matrix(max_3)
    users_matching_result=get_users_matching_result(integrated_similarity_matrix)
    return users_matching_result

def read_csv(path):
    temp = csv.reader(open(path), delimiter=',')
    old_matrix = np.array(list(temp)).astype('int64')
    return old_matrix

#for RPC
def match(feature_list):
    result=UserMatching(np.array(feature_list).astype('int64'))
    return result.tolist()

if __name__ == "__main__":
    feature_matrix=read_csv('test.csv')
    result=UserMatching(feature_matrix)
    np.savetxt('result.txt',result,'%d',',')

