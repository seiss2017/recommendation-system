'''
#python3 version

from xmlrpc import client
import numpy as np
import csv

server = client.ServerProxy("http://localhost:8000")
match = server.match

print("reading test file...")
lis = list(csv.reader(open("test.csv"), delimiter=','))
print("finished reading test file")

print("matching users...")
result=match(lis)
print("finished matching users")
print("saving result...")
np.savetxt('result.txt',result,'%d',',')
print("finished saving result.")
'''

#python2 version
import xmlrpclib
import numpy as np
import csv

server = xmlrpclib.ServerProxy("http://localhost:8000")
match = server.match
print("reading test file...")
lis = list(csv.reader(open("test.csv"), delimiter=','))
print("finished reading test file")
print("matching users...")
result=match(lis)
print("finished matching users")
print("saving result...")
np.savetxt('result.txt',result,'%d',',')
print("finished saving result.")
