# Description #
-------------

UserMatching.py is the implementation of the recommendation system of LTalk (Using Python2.7).

# Dependency #
For using this package, some dependency packages are needed:

*  1.numpy
*  2.CVXOPT (http://cvxopt.org/install/index.html)
*  3.scipy

# Usage #
For getting the recommendation of users, you only need to call the function: *UserMatching(feature_matrix)*

The only argument of this function is the feature_matrix (type: np.array), which means the association matrix (or interaction matrix) between Users and their corresponding Interests.

For example:

*     3 Users: A,B,C
*     4 Interests: book, basketball, computer, talking
*     Description: A loves book, computer
                   B loves basketball, computer, talking
                   C loves talking
*     The feature matrix will be constructed:
                 [[1 0 1 0]
                 [0 1 1 1]
                 [0 0 0 1]]

After calling the function UserMatching, you will get the best matching result (other users' Index) of each user.
The result will be formatted into an X*X matrix, X is the user total number.

# Demo #
An simply example is given. 'test.csv' contains the test data.

# Contact #
For any questions when using the package, please contact Tommy (Xiang Yue): tommy96@whu.edu.cn