'''
#python3 version

from xmlrpc import server
import numpy as np
#from UserMatching import match

def match(li):
    return li

print("Listening on port 8000...")
server = server.SimpleXMLRPCServer(("localhost", 8000))

server.register_function(match)
server.register_multicall_functions()
server.serve_forever()
'''
#python2 version
from SimpleXMLRPCServer import SimpleXMLRPCServer
from UserMatching import match

print "Listening on port 8000..."
server=SimpleXMLRPCServer(('localhost', 8000))
server.register_function(match)
server.serve_forever()
